//
//  ApiServices.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 10/22/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import CoreLocation

class ApiServices: NSObject {
    
    static let shared = ApiServices()
    let firebaseDatabase = Database.database().reference()
    
    func fetchFireData(completion: @escaping ([(long: Double,lat: Double,brightness: Double,conf:Double,date:Date,dayNight:String)]) -> Void){
        
        let request: URLRequest? = {
            guard let url = URL(string: "https://firms.modaps.eosdis.nasa.gov/data/active_fire/c6/csv/MODIS_C6_Europe_24h.csv") else {print("Fail to acess data");return nil}
            let req = URLRequest(url: url)
            return req
        }()
        
        guard let req = request else {return}
        
        
        
        let task = URLSession.shared.dataTask(with: req) { (data, res, err) in
            var fireDataFromApi = [(long: Double,lat: Double,brightness: Double,conf:Double,date:Date,dayNight:String)]()
            if let er = err {
                print("error:\(er)")
                return
            }
            let text = String(data: data!, encoding: String.Encoding.ascii)!
            
            let lines = text.split(separator: Character("\n"))
            
            
            for (i,line) in lines.enumerated(){
                let splitLine = line.split(separator: Character(","))
                
                if i != 0 {
                    
                    let lat = Double(splitLine[0])
                    let long = Double(splitLine[1])
                    let bright = Double(splitLine[2])
                    let conf = Double(splitLine[8])
                    let date = String(splitLine[5]).convertStringToDate()
                    let dayNight = String(splitLine[12])
                    
                    
                    
                    fireDataFromApi.append((long: long!,lat: lat!,brightness: bright!,conf:conf!,date:date,dayNight:dayNight))
                    
                }
                
                
            }
            
            completion(fireDataFromApi)
  
        }
        
        task.resume()
    }
    
    
    // Firebase connections...
    
    
    func addFirebaseDataLocations(coordinates: CLLocationCoordinate2D){
        let data: NSDictionary = ["latitude":coordinates.latitude,"longitude":coordinates.longitude,"timestamp":Date().timeIntervalSince1970]
        firebaseDatabase.child("Fires").childByAutoId().setValue(data)
    }
    
    
    
    
    
    func retrieveFirebaseData(completion:@escaping ([(coordinates:CLLocationCoordinate2D,timestamp:TimeInterval)])->Void){
        firebaseDatabase.child("Fires").observe(DataEventType.value) { (snap) in
            var fireLocationsWithDate = [(coordinates:CLLocationCoordinate2D,timestamp:TimeInterval)]()
            if let data = snap.value as? [String:Any] {
                for item in data {
                    if let coordinates = item.value as? [String:Any] {
                        let timeStamp = coordinates["timestamp"] as! Double
                        let latitude = coordinates["latitude"] as! Double
                        let longitude = coordinates["longitude"] as! Double
                        print(timeStamp,latitude,longitude)
                        fireLocationsWithDate.append((coordinates: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), timestamp: timeStamp))
                    }
                }
                completion(fireLocationsWithDate)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}
