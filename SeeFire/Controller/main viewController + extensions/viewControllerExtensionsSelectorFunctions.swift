//
//  viewControllerExtensionsSelectorFunctions.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 11/4/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation


// actions ans selector for UI elements...
extension ViewController {
    
    @objc func iSeeFireAction(sender: UIButton) {
        darkView = {
            let dark = UIView()
            dark.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            dark.alpha = 0
            dark.frame = self.view.frame
            dark.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissContainerViewForFireButton)))
            return dark
        }()
        
        mapkitView.insertSubview(darkView, belowSubview: containerViewForSeeFireButton)
        
        
        
        topAnchorSeeFireContainerView.constant = -(view.frame.height * 0.25 + 15)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            self.darkView.alpha = 1
        }, completion: nil)
        
    }
    
    @objc func dismissContainerViewForFireButton(sender: UITapGestureRecognizer){
        if !isReporting {
            reportMode = .none
            containerViewForSeeFireButton.subviews.forEach{ (buttonMode) in
                if let button = buttonMode as? UIButton{
                    button.layer.borderWidth = 1
                    button.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                }
            }
        }
        
        topAnchorSeeFireContainerView.constant = 5
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            sender.view?.alpha = 0
        }, completion: { (_) in
            sender.view?.removeFromSuperview()
        })
    }
    
    
    
    @objc func handleInfoButton(){
        
        let darkView: UIView = {
            let dark = UIView()
            dark.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            dark.frame = mapkitView.frame
            dark.alpha = 0
            dark.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissDarkViewAndContainerView)))
            return dark
        }()
        
        mapkitView.insertSubview(darkView, belowSubview: containerViewForInfoButton)
        
        containerViewTopAnchorConstraint.constant = -115
        UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            darkView.alpha = 1
        }, completion: nil)
        
    }
    
    @objc func dismissDarkViewAndContainerView(sender: UITapGestureRecognizer){
        
        containerViewTopAnchorConstraint.constant = 5
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
            sender.view?.alpha = 0
        }) { (sucess) in
            sender.view?.removeFromSuperview()
        }
        
    }
    
    
    @objc func handleCentraliseButton(){
        guard let currentLocation = locatManager.location else {return}
        UIView.animate(withDuration: 1, animations: {
            self.mapkitView.setCenter(currentLocation.coordinate, animated: true )
            self.mapkitView.region = MKCoordinateRegion(center: currentLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        }, completion: nil)
    }
    
    
    
    @objc func handleSegmentedControl(sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            print("mapMode")
            mapkitView.mapType = MKMapType.standard
        } else if sender.selectedSegmentIndex == 1 {
            print("satelliteMode")
            mapkitView.mapType = MKMapType.satellite
        } else {
            print("HybridMode")
            mapkitView.mapType = MKMapType.hybrid
        }
    }

    
    
    
    
}
