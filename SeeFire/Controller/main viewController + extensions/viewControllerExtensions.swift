//
//  viewControllerExtensions.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 11/4/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UserNotifications
import CoreLocation
import UIKit
import FirebaseDatabase
import MapKit


// views and constraints....
extension ViewController {
    
    func setMapViewConstraints(){
        
        firebaseDatabase = Database.database().reference()
        centraliseButton = {
            let button = UIButton()
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setImage(#imageLiteral(resourceName: "centralizeButton"), for: .normal)
            button.contentMode = UIView.ContentMode.scaleAspectFit
            button.contentHorizontalAlignment = .fill
            button.contentVerticalAlignment = .fill
            button.addTarget(self, action: #selector(handleCentraliseButton), for: .touchUpInside)
            return button
        }()
        
        infoButton = {
            let button = UIButton()
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setImage(#imageLiteral(resourceName: "InfoButton"), for: .normal)
            button.contentMode = UIView.ContentMode.scaleAspectFit
            button.contentHorizontalAlignment = .fill
            button.contentVerticalAlignment = .fill
            button.addTarget(self, action: #selector(handleInfoButton), for: .touchUpInside)
            return button
        }()
        
        seeFireButton = {
            let button = UIButton()
            button.translatesAutoresizingMaskIntoConstraints = false //Makes the coinstraint working
            button.setImage(#imageLiteral(resourceName: "firebutton"), for: .normal)
            button.contentMode = UIView.ContentMode.scaleAspectFit
            button.contentHorizontalAlignment = .fill
            button.contentVerticalAlignment = .fill
            button.layer.cornerRadius = 40
            button.addTarget(self, action: #selector(iSeeFireAction), for: .touchUpInside)
            return button
        }()
        
        locatManager = {
            let manager = CLLocationManager()
            manager.delegate = self
            manager.requestAlwaysAuthorization()
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
            return manager
        }()
        
        mapkitView = {
            let map = MKMapView()
            map.delegate = self
            map.register(customAnnotation.self, forAnnotationViewWithReuseIdentifier: "customAnnotation")
            map.register(reportedFireAnnotations.self, forAnnotationViewWithReuseIdentifier: "reportedFireAnnotation")
            let regionRadius: CLLocationDistance = 2000
            map.translatesAutoresizingMaskIntoConstraints = false
            map.userTrackingMode = MKUserTrackingMode.follow
            if let userLocation = locatManager.location?.coordinate {
                map.centerCoordinate = CLLocationCoordinate2D(latitude: userLocation.latitude, longitude: userLocation.longitude)
                let coordinateRegion = MKCoordinateRegion(center: userLocation,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
                map.setRegion(coordinateRegion, animated: true)
                map.showsUserLocation = true
                map.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(reportNewFire)))
                return map
            }
            return map
        }()
        
        containerViewForInfoButton = {
            let container = UIView()
            container.translatesAutoresizingMaskIntoConstraints = false
            container.layer.borderWidth = 1
            container.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
            container.layer.cornerRadius = 30
            container.backgroundColor = UIColor(red: 68/255, green: 76/255, blue: 83/255, alpha: 1.0)
            
            let segmented = UISegmentedControl(items: ["Map","Satellite","Hybrid"])
            segmented.translatesAutoresizingMaskIntoConstraints = false
            segmented.tintColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
            segmented.addTarget(self, action: #selector(handleSegmentedControl), for: .valueChanged)
            segmented.selectedSegmentIndex = 0
            container.addSubview(segmented)
            
            segmented.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.8).isActive = true
            segmented.heightAnchor.constraint(equalToConstant: 40).isActive = true
            segmented.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
            segmented.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
            
            
            return container
        }()
        
        

        containerViewForSeeFireButton = {
            let container = UIView()
            container.translatesAutoresizingMaskIntoConstraints = false
            container.layer.borderWidth = 1
            container.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
            container.layer.cornerRadius = 30
            container.clipsToBounds = true
            container.backgroundColor = UIColor(red: 68/255, green: 76/255, blue: 83/255, alpha: 1.0)
            
            let gradient = CAGradientLayer()
            gradient.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
            gradient.locations = [0,1]
            gradient.opacity = 1
            gradient.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 60))
            
            // Button Report Fire
            let reportFireButton =  UIButton()
            reportFireButton.translatesAutoresizingMaskIntoConstraints = false
            reportFireButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
            reportFireButton.layer.addSublayer(gradient)
            reportFireButton.addTarget(self, action: #selector(handleReportButton), for: .touchUpInside)
            reportFireButton.layer.masksToBounds = true
            reportFireButton.layer.borderWidth = 1
            reportFireButton.layer.cornerRadius = 30
            reportFireButton.setTitle("REPORT FIRE", for: .normal)
            
            // Button I See Smoke
            let iSeeSmokebutton = UIButton()
            iSeeSmokebutton.translatesAutoresizingMaskIntoConstraints = false //Makes the coinstraint working
            iSeeSmokebutton.setImage(#imageLiteral(resourceName: "Smoke icon"), for: .normal)
            iSeeSmokebutton.tag = 1
            iSeeSmokebutton.addTarget(self, action: #selector(changeReportMode), for: .touchUpInside)
            iSeeSmokebutton.layer.borderWidth = 1
            iSeeSmokebutton.layer.borderColor = UIColor.orange.cgColor
            iSeeSmokebutton.titleLabel?.text = "Smoke"
            
            // Button I See Small Fire
            let iSeeSmallFirebutton = UIButton()
            iSeeSmallFirebutton.translatesAutoresizingMaskIntoConstraints = false
            iSeeSmallFirebutton.setImage(#imageLiteral(resourceName: "smallFireIcon"), for: .normal)
            iSeeSmallFirebutton.tag = 2
            iSeeSmallFirebutton.addTarget(self, action: #selector(changeReportMode), for: .touchUpInside)
            iSeeSmallFirebutton.layer.borderWidth = 1
            iSeeSmallFirebutton.layer.borderColor = UIColor.orange.cgColor
            iSeeSmallFirebutton.titleLabel?.text = "Small Fire"
            
            // Button I See Big Fire
            let iSeeBigFirebutton = UIButton()
            iSeeBigFirebutton.translatesAutoresizingMaskIntoConstraints = false
            iSeeBigFirebutton.setImage(#imageLiteral(resourceName: "bigFireIcon"), for: .normal)
            iSeeBigFirebutton.tag = 3
            iSeeBigFirebutton.addTarget(self, action: #selector(changeReportMode), for: .touchUpInside)
            iSeeBigFirebutton.layer.borderWidth = 1
            iSeeBigFirebutton.layer.borderColor = UIColor.orange.cgColor
            
            // Adding alements
            container.addSubview(reportFireButton)
            container.addSubview(iSeeSmokebutton)
            container.addSubview(iSeeSmallFirebutton)
            container.addSubview(iSeeBigFirebutton)
            
            // Button Report Fire Contrainsts
            reportFireButton.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.9).isActive = true
            reportFireButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
            reportFireButton.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
            reportFireButton.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20).isActive = true
            
            // Button I See Small Fire Constraints
            iSeeSmallFirebutton.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 1/3).isActive = true
            iSeeSmallFirebutton.heightAnchor.constraint(equalToConstant: 100).isActive = true
            iSeeSmallFirebutton.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
            iSeeSmallFirebutton.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
            
            // Button I See Smoke Constraints
            iSeeSmokebutton.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 1/3).isActive = true
            iSeeSmokebutton.heightAnchor.constraint(equalToConstant: 100).isActive = true
            iSeeSmokebutton.rightAnchor.constraint(equalTo: iSeeSmallFirebutton.leftAnchor).isActive = true
            iSeeSmokebutton.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
            
            // Button I See Big Fire Constraints
            iSeeBigFirebutton.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 1/3).isActive = true
            iSeeBigFirebutton.heightAnchor.constraint(equalToConstant: 100).isActive = true
            iSeeBigFirebutton.leftAnchor.constraint(equalTo: iSeeSmallFirebutton.rightAnchor).isActive = true
            iSeeBigFirebutton.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
            
            return container
        }()
        
        cancelReportModebutton = {
            let button = UIButton()
            let gradient = CAGradientLayer()
            gradient.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
            gradient.locations = [0,1]
            gradient.opacity = 1
            gradient.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 50))
            button.alpha = 0
            button.layer.cornerRadius = 15
            button.clipsToBounds = true
            button.layer.addSublayer(gradient)
            button.addTarget(self, action: #selector(cancelReportMode), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.backgroundColor = .red
            button.setTitle("Cancel", for: .normal)
            return button
        }()
        
        performReportToFirebaseButton = {
            let button = UIButton()
            let gradient = CAGradientLayer()
            gradient.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
            gradient.locations = [0,1]
            gradient.opacity = 1
            gradient.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 50))
            button.alpha = 0
            button.layer.cornerRadius = 15
            button.clipsToBounds = true
            button.layer.addSublayer(gradient)
            button.addTarget(self, action: #selector(reportToFirebase), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.backgroundColor = .red
            button.setTitle("Report", for: .normal)
            return button
        }()
        
        view.addSubview(mapkitView)
        mapkitView.addSubview(warningInfoLabel)
        mapkitView.addSubview(seeFireButton)
        mapkitView.addSubview(centraliseButton)
        mapkitView.addSubview(infoButton)
        mapkitView.addSubview(containerViewForInfoButton)
        mapkitView.addSubview(containerViewForSeeFireButton)
        
        //******** Map Kit Contrainsts ********
        mapkitView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapkitView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        mapkitView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        mapkitView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        //******** Centralise Button Contrainsts ********
        
        leftAnchorCentraliseButton = centraliseButton.leftAnchor.constraint(equalTo: mapkitView.leftAnchor, constant: 20)
        leftAnchorCentraliseButton.isActive = true
        centraliseButton.topAnchor.constraint(equalTo: mapkitView.topAnchor, constant: 50).isActive = true
        centraliseButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        centraliseButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //******** Info Button Contrainsts ********
        infoButton.topAnchor.constraint(equalTo: centraliseButton.bottomAnchor, constant: 20).isActive = true
        infoButton.centerXAnchor.constraint(equalTo: centraliseButton.centerXAnchor).isActive = true
        infoButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
        infoButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        //******** See Fire Button Contrainsts ********
        bottomAnchorSeeFireButton = seeFireButton.bottomAnchor.constraint(equalTo: mapkitView.bottomAnchor, constant: -20)
        bottomAnchorSeeFireButton.isActive = true
        seeFireButton.rightAnchor.constraint(equalTo: mapkitView.rightAnchor, constant: -20).isActive = true
        seeFireButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        seeFireButton.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        //******** ContainerViewTopAnchor Contrainsts ********
        containerViewTopAnchorConstraint = containerViewForInfoButton.topAnchor.constraint(equalTo: mapkitView.bottomAnchor, constant: 5)
        containerViewTopAnchorConstraint.isActive = true
        containerViewForInfoButton.centerXAnchor.constraint(equalTo: mapkitView.centerXAnchor).isActive = true
        containerViewForInfoButton.widthAnchor.constraint(equalTo: mapkitView.widthAnchor, multiplier: 0.95).isActive = true
        containerViewForInfoButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        
        
        //******** ContainerSeeFire Contrainsts ********
        containerViewForSeeFireButton.widthAnchor.constraint(equalTo: mapkitView.widthAnchor, multiplier: 0.95).isActive = true
        containerViewForSeeFireButton.heightAnchor.constraint(equalTo: mapkitView.heightAnchor, multiplier: 0.25).isActive = true
        topAnchorSeeFireContainerView = containerViewForSeeFireButton.topAnchor.constraint(equalTo: mapkitView.bottomAnchor, constant: 5)
        topAnchorSeeFireContainerView.isActive = true
        containerViewForSeeFireButton.centerXAnchor.constraint(equalTo: mapkitView.centerXAnchor).isActive = true
        
        warningInfoLabel.widthAnchor.constraint(equalTo: mapkitView.widthAnchor, multiplier: 0.8).isActive = true
        warningInfoLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0).isActive = true
        warningInfoLabel.centerXAnchor.constraint(equalTo: mapkitView.centerXAnchor).isActive = true
        warningInfoLabel.topAnchor.constraint(equalTo: mapkitView.topAnchor, constant: 50).isActive = true
        
    }
    
}

