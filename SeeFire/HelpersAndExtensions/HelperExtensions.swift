//
//  HelperExtensions.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 10/22/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    
    func convertStringToDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: self ) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        return date
    }
    

}


class customLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
//    override var safeAreaInsets: UIEdgeInsets {
//        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//    }
    
    
    func setUp(){
        lineBreakMode = NSLineBreakMode.byWordWrapping
        numberOfLines = 0
        textColor = UIColor.white
        textAlignment = .center
        layer.cornerRadius = 8
        clipsToBounds = true
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        font = UIFont.boldSystemFont(ofSize: 20)
        translatesAutoresizingMaskIntoConstraints = false
        alpha = 0
    }
    
    
    override func drawText(in rect: CGRect) {
        let newRect = CGRect.insetBy(rect)
        super.drawText(in: newRect(5,0))
    }

}
