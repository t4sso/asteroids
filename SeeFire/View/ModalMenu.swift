//
//  ModalMenu.swift
//  SeeFire
//
//  Created by Amanda Aurita Araujo Fernandes on 20/10/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import UIKit

class ModalMenu: UIPresentationController {
     func frameOfPresentedViewInContainerView() -> CGRect {
        return CGRect(x: 0, y: 0, width: containerView!.bounds.width, height: (containerView?.bounds.height)!/2)
    }
}
