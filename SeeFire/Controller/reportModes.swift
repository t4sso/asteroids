//
//  reportModes.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 11/3/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation


enum reportModes: String {
    case smoke
    case smallFire
    case bigFire
    case none
}



