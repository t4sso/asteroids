//
//  Fire.swift
//  SeeFire
//
//  Created by Amanda Aurita Araujo Fernandes on 20/10/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation


class Fire{
    
    
    var lat: Float!
    var long: Float!
    var bright: Float!
    var conf: Float!
    var data: String!
    var time: String!
    
    init(la: Float, lo:Float, br: Float, co: Float, d: String, t: String ){
        self.lat = la
        self.long = lo
        self.bright = br
        self.conf = co
        self.data = d
        self.time = t
    }
    
    
    
}
