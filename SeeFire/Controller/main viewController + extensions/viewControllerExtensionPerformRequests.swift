//
//  viewControllerExtensionPerformRequests.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 11/4/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import MapKit


// perform all requests here

extension ViewController {
    
    func performFirebaseDataRequest(){
        ApiServices.shared.retrieveFirebaseData { (dataFromFirebase) in
            print("firebase Data count:",dataFromFirebase.count)
            self.reportedFiresData = dataFromFirebase
            self.updateUserReportedFiresOnMap()
            var locations = [CLLocation]()
            
            for reportedFire in self.reportedFiresData {
                let location = CLLocation(latitude: reportedFire.coordinates.latitude, longitude: reportedFire.coordinates.longitude)
                locations.append(location)
            }
            guard let currentLocation = self.locatManager.location else {return}
            self.evaluateReportedFiresNearby(userLocation: currentLocation, reportedLocations: locations)
        }
    }
    
    
    
    func performNasaApiDataRequest(){
        ApiServices.shared.fetchFireData { (fireDataResponse) in
            self.fireData = fireDataResponse
            
            self.updateFireMarkersOnMap()
            var locations = [CLLocation]()
            
            for fire in self.fireData {
                let location = CLLocation(latitude: fire.lat, longitude: fire.long)
                locations.append(location)
            }
            
            guard let currentLocation = self.locatManager.location else {return}
            self.evaluateFireNearby(userLocation: currentLocation,fireLocations:locations)
            
        }
    }
    
    
    func updateUserReportedFiresOnMap(){
        for annot in mapkitView.annotations {
            if annot.title == "reportedFires" {
                mapkitView.removeAnnotation(annot)
            }
        }
        
        for data in reportedFiresData {
            let annotation = MKPointAnnotation()
            annotation.title = "reportedFires"
            annotation.coordinate = CLLocationCoordinate2D(latitude: data.coordinates.latitude, longitude: data.coordinates.longitude)
            mapkitView.addAnnotation(annotation)
        }
    }
    
    
    
    
    @objc func updateFireMarkersOnMap(){
        print("data count:",fireData.count)
        for annot in mapkitView.annotations {
            if annot.title == "FireIndentifierSatellite" {
                mapkitView.removeAnnotation(annot)
            }
        }
        
        for data in fireData {
            let annotation = MKPointAnnotation()
            annotation.title = "FireIndentifierSatellite"
            annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(data.lat), longitude: CLLocationDegrees(data.long))
            mapkitView.addAnnotation(annotation)
        }
    }
    
    func evaluateReportedFiresNearby(userLocation: CLLocation, reportedLocations:[CLLocation]){
        var nearbyReportedFires = [CLLocation]()
        mapkitView.overlays.forEach { (reportedOverlay) in
            if reportedOverlay.title == "reportedFireData"{
                mapkitView.removeOverlay(reportedOverlay)
            }
        }
        locatManager.monitoredRegions.forEach { (region) in
            if region.identifier.contains(Character("R")){
                self.locatManager.stopMonitoring(for: region)
            }
        }
        
        
        for item in reportedLocations {
            let distance = userLocation.distance(from: item)
            if distance < minimunDistance {
                nearbyReportedFires.append(item)
            }
        }
        
        
        for (i,report) in nearbyReportedFires.enumerated(){
            let fireCenter = report.coordinate
            let circularRegion = CLCircularRegion(center: fireCenter, radius: radiusForGeofenceInMeters, identifier: String(i)+"R")
            circularRegion.notifyOnExit = true
            circularRegion.notifyOnEntry = true
            
            self.locatManager.startMonitoring(for: circularRegion)
            
            let geofenceView = MKCircle(center: fireCenter, radius: circularRegion.radius)
            geofenceView.title = "reportedFireData"
            DispatchQueue.main.async {
                self.mapkitView.addOverlay(geofenceView)
            }
            
        }
        
        if nearbyReportedFires.count > 0 {
            numberOfReportedFiresNearby = nearbyReportedFires.count
        }
        
    }
    
    
    func evaluateFireNearby(userLocation: CLLocation,fireLocations:[CLLocation]){
        var nearbyFireLocations = [CLLocation]()
        mapkitView.overlays.forEach { (overL) in
            if overL.title == "nasaFireData"{
                mapkitView.removeOverlay(overL)
            }
        }
        locatManager.monitoredRegions.forEach { (region) in
            if region.identifier.contains(Character("N")){
                self.locatManager.stopMonitoring(for: region)
            }
        }
        
        
        
        
        for item in fireLocations {
            let distance = userLocation.distance(from: item)
            
            if distance < minimunDistance {
                
                nearbyFireLocations.append(item)
                
            }
            
        }
        
        for (i,fire) in nearbyFireLocations.enumerated(){
            let fireCenter = fire.coordinate
            let circularRegion = CLCircularRegion(center: fireCenter, radius: radiusForGeofenceInMeters, identifier: String(i)+"N")
            circularRegion.notifyOnExit = true
            circularRegion.notifyOnEntry = true
            
            self.locatManager.startMonitoring(for: circularRegion)
            
            let geofenceView = MKCircle(center: fireCenter, radius: circularRegion.radius)
            geofenceView.title = "nasaFireData"
            DispatchQueue.main.async {
                self.mapkitView.addOverlay(geofenceView)
            }
        }
        
        if nearbyFireLocations.count > 0 {
            numberOfFiresNearbyByNasa = nearbyFireLocations.count
            
        }
        
    }
    
    
}
