//
//  customPointer.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 10/20/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UIKit
import MapKit



class customAnnotation: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        setPointer()
    }
    
    
   
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    
    
    func setPointer(){
        
            image = #imageLiteral(resourceName: "firebutton")
            backgroundColor = UIColor.clear
            frame.size = CGSize(width: 20, height: 20)
            isUserInteractionEnabled = true

    }
  
    
}

class reportedFireAnnotations: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        setPointer()
    }
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    func setPointer(){
        
        image = #imageLiteral(resourceName: "targetView2")
        backgroundColor = UIColor.clear
        frame.size = CGSize(width: 20, height: 20)
        isUserInteractionEnabled = true
        
    }
    
    
}



