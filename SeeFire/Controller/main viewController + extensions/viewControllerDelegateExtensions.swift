//
//  viewControllerDelegateExtensions.swift
//  SeeFire
//
//  Created by Lucas Alves da Silva on 11/4/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import UserNotifications
import CoreLocation
import MapKit



extension ViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    
    // LOCATION DELEGATES...
        func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
            let content = UNMutableNotificationContent()
            content.title = "Report"
            content.body = "you're safe now, you just left the risk area"
            content.sound = UNNotificationSound.default
            let request = UNNotificationRequest(identifier: "didExitRiskArea", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request) { (error) in
                if let er = error {print(er.localizedDescription)}
            }
            
        }
        
        func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
            
            let content = UNMutableNotificationContent()
            content.title = "Report"
            content.body = "you've entered a risk area, there's a fire nearby"
            content.sound = UNNotificationSound.default
            let request = UNNotificationRequest(identifier: "didEnterRiskArea", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request) { (error) in
                if let er = error {print(er.localizedDescription)}
            }
            
        }
        
    

    
    
    // MAP KIT DELEGATES
    
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
            if annotation is MKUserLocation {
                return nil
            }
            
            if annotation.title == "FireIndentifierSatellite" {
                let annotation = mapkitView.dequeueReusableAnnotationView(withIdentifier: "customAnnotation") as! customAnnotation
                return annotation
            } else {
                let annotation = mapkitView.dequeueReusableAnnotationView(withIdentifier: "reportedFireAnnotation") as! reportedFireAnnotations
                return annotation
            }
            
        }
        
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            
            guard let circularOverlay = overlay as? MKCircle else {return MKOverlayRenderer()}
            
            
            let renderer = MKCircleRenderer(overlay: circularOverlay)
            renderer.strokeColor = .red
            renderer.fillColor = .yellow
            renderer.alpha = 0.3
            renderer.lineWidth = 2
            
            return renderer
        }
        
    

    
}
