//
//  ViewController.swift
//  SeeFire
//
//  Created by Amanda Aurita Araujo Fernandes on 20/10/18.
//  Copyright © 2018 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import UIKit
import MapKit
import UserNotifications

class ViewController: UIViewController, UIViewControllerTransitioningDelegate {
    // minimum distance to alert user about fires in meters
    let minimunDistance:Double = 500
    let radiusForGeofenceInMeters: Double = 500
    
    var numberOfReportedFiresNearby = 0 {
        didSet{
            totalFiresNearby = numberOfFiresNearbyByNasa + numberOfReportedFiresNearby
        }
    }
    
    var numberOfFiresNearbyByNasa = 0 {
        didSet{
            totalFiresNearby = numberOfFiresNearbyByNasa + numberOfReportedFiresNearby
        }
    }
    
    var totalFiresNearby = 0 {
        didSet{
            DispatchQueue.main.async {
                let verbToBe = self.totalFiresNearby == 1 ? "is" : "are"
                self.showAlertModal(title: "FIRE REPORT!", Message: "There \(verbToBe) \(self.totalFiresNearby) fire reports nearby. Check your surroundings.")
            }
        }
    }
    
    
    
    
    
    var fireData = [(long: Double,lat: Double,brightness: Double,conf:Double,date:Date,dayNight:String)]()
    var reportedFiresData = [(coordinates: CLLocationCoordinate2D, timestamp: TimeInterval)]()
    
    // constraint reference for containerViewForInfoButton, will be used for animating the view's Y position after infoButton is called...
    var containerViewTopAnchorConstraint: NSLayoutConstraint!
    var topAnchorSeeFireContainerView: NSLayoutConstraint!
    var bottomAnchorSeeFireButton: NSLayoutConstraint!
    var leftAnchorCentraliseButton: NSLayoutConstraint!

    
    // latitude and longitude when user taps on a point on the map... will be used to report new fires.. it's updated on delegate method touchesEnded...
    var touchCoordinates = CLLocationCoordinate2D()
    
    var reportMode: reportModes = .none
    var isReporting = false

    // All set up is made in the extension file inside the function 'setMapViewConstraints()' and called in viewDidLoad...
    
    var firebaseDatabase: DatabaseReference!
    lazy var centraliseButton: UIButton = UIButton()
    lazy var infoButton: UIButton = UIButton()
    lazy var seeFireButton: UIButton = UIButton()
    lazy var locatManager: CLLocationManager = CLLocationManager()
    lazy var mapkitView: MKMapView = MKMapView()
    lazy var containerViewForInfoButton: UIView = UIView()
    lazy var containerViewForSeeFireButton: UIView = UIView()
    lazy var cancelReportModebutton: UIButton = UIButton()
    lazy var performReportToFirebaseButton: UIButton = UIButton()
    
    var warningInfoLabel = customLabel()

    // used for animation after reportButton is clicked by the user...
    let redFrameForMapKit = CAShapeLayer()
    var darkView: UIView!
    
    lazy var temporaryViewForReporting = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        locatManager.monitoredRegions.forEach { (reg) in
            print("region monitored:",reg)
            
        }

        
        
        setMapViewConstraints()
        performNasaApiDataRequest()
        performFirebaseDataRequest()
    }
    
    

    
    
  
  
    
    func showAlertModal(title: String, Message: String){
        let alert = UIAlertController(title: title, message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (_) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
   
  
    
    
    

    @objc func reportNewFire(touch: UILongPressGestureRecognizer){
        let location = touch.location(in: mapkitView)
        let touchedCoordinates = mapkitView.convert(location, toCoordinateFrom: mapkitView)
        touchCoordinates = touchedCoordinates
        
        
        if isReporting {
            print(touchedCoordinates)
            switch touch.state {
                case .began:
                    temporaryViewForReporting.image = #imageLiteral(resourceName: "targetView")
                    temporaryViewForReporting.alpha = 1
                    temporaryViewForReporting.frame.size = CGSize(width: 80, height: 80)
                    mapkitView.addSubview(temporaryViewForReporting)
                    UIView.animate(withDuration: 0.2) {
                        self.temporaryViewForReporting.center = location
                    }
  
                case .changed:
                    temporaryViewForReporting.center = location
     
                case .ended:
                    print("long ended")
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.temporaryViewForReporting.alpha = 0
                    }) { (_) in
                        ApiServices.shared.addFirebaseDataLocations(coordinates: touchedCoordinates)
                        self.temporaryViewForReporting.removeFromSuperview()
                    }

                default:
                    print("long default")
            }
        }
    }
}

























// extension for  @objc functions to handle button actions...
extension ViewController {
    
    @objc func handleReportButton(sender:UIButton){
        if reportMode == .none {
            showAlertModal(title: "Mode Required!", Message: "Please, select one of the modes:\n smoke, small fire or big fire")
            return
        }
        
        isReporting = true
        
        leftAnchorCentraliseButton.constant = -100
        topAnchorSeeFireContainerView.constant = 5
        bottomAnchorSeeFireButton.constant = 100
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            self.darkView.alpha = 0
            self.warningInfoLabel.alpha = 1
            self.cancelReportModebutton.alpha = 1
            self.performReportToFirebaseButton.alpha = 1
        })
        print(mapkitView.frame)
        let bezierPath = UIBezierPath(rect: CGRect(origin: .zero, size: CGSize(width: mapkitView.frame.width , height: mapkitView.frame.height )))
        redFrameForMapKit.fillColor = UIColor.clear.cgColor
        redFrameForMapKit.path = bezierPath.cgPath
        redFrameForMapKit.strokeColor = UIColor.red.cgColor
        redFrameForMapKit.lineWidth = 30
        let animation = addPulsingAnimation()
        redFrameForMapKit.add(animation, forKey: "blink")
        
        warningInfoLabel.text = "Report Mode: ON\nplease select the place on the map where you saw the \(reportMode.rawValue)"
        print(warningInfoLabel.safeAreaInsets)
        
        
        mapkitView.addSubview(cancelReportModebutton)
        mapkitView.addSubview(performReportToFirebaseButton)
        mapkitView.addSubview(warningInfoLabel)
        mapkitView.layer.addSublayer(redFrameForMapKit)
        
        cancelReportModebutton.widthAnchor.constraint(equalTo: mapkitView.widthAnchor, multiplier: 0.4).isActive = true
        cancelReportModebutton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cancelReportModebutton.bottomAnchor.constraint(equalTo: mapkitView.bottomAnchor, constant: -30).isActive = true
        cancelReportModebutton.leftAnchor.constraint(equalTo: mapkitView.leftAnchor, constant: 30).isActive = true
        
        performReportToFirebaseButton.widthAnchor.constraint(equalTo: mapkitView.widthAnchor, multiplier: 0.4).isActive = true
        performReportToFirebaseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        performReportToFirebaseButton.centerYAnchor.constraint(equalTo: cancelReportModebutton.centerYAnchor).isActive = true
        performReportToFirebaseButton.rightAnchor.constraint(equalTo: mapkitView.rightAnchor, constant: -30).isActive = true
    }
    
    func addPulsingAnimation() -> CAAnimation{
        let animation = CAKeyframeAnimation(keyPath: #keyPath(CAShapeLayer.opacity))
        animation.duration = 0.5
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.values = [0,1]
        animation.keyTimes = [0,1]
        
        return animation
    }
    
    @objc func reportToFirebase(){
        print("report to firebase")
        
    }
    
    @objc func cancelReportMode(){
        reportMode = .none
        isReporting = false
        self.redFrameForMapKit.removeAllAnimations()
        self.redFrameForMapKit.removeFromSuperlayer()
        self.bottomAnchorSeeFireButton.constant = -20
        self.leftAnchorCentraliseButton.constant = 20
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            self.warningInfoLabel.alpha = 0
            self.cancelReportModebutton.alpha = 0
            self.performReportToFirebaseButton.alpha = 0
            self.redFrameForMapKit.opacity = 0
        }) { (_) in
            
            self.containerViewForSeeFireButton.subviews.forEach({ (view) in
                let button = view as! UIButton
                button.layer.borderWidth = 1
                button.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
            })
           
        }
     
    }
    
    @objc func changeReportMode(sender:UIButton){
        
        var smokeButton: UIButton = UIButton()
        var smallFireButton: UIButton = UIButton()
        var bigFireButton: UIButton = UIButton()
        
        containerViewForSeeFireButton.subviews.forEach{ (buttonMode) in
           
            
            if buttonMode.tag == 1 {smokeButton = buttonMode as! UIButton}
            if buttonMode.tag == 2 {smallFireButton = buttonMode as! UIButton}
            if buttonMode.tag == 3 {bigFireButton = buttonMode as! UIButton}

        }
            
            if (sender.tag == 1){
                // smoke...
                reportMode = .smoke
                
                smokeButton.layer.borderWidth = 12
                smokeButton.layer.borderColor = UIColor.red.cgColor
                smallFireButton.layer.borderWidth = 1
                smallFireButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                bigFireButton.layer.borderWidth = 1
                bigFireButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                
            } else if (sender.tag == 2){
                // small fire...
                reportMode = .smallFire
                
                smokeButton.layer.borderWidth = 1
                smokeButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                smallFireButton.layer.borderWidth = 12
                smallFireButton.layer.borderColor = UIColor.red.cgColor
                bigFireButton.layer.borderWidth = 1
                bigFireButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                
            } else if (sender.tag == 3){
                // big fire...
                reportMode = .bigFire
                
                smokeButton.layer.borderWidth = 1
                smokeButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                smallFireButton.layer.borderWidth = 1
                smallFireButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.5490196078, blue: 0, alpha: 1)
                bigFireButton.layer.borderWidth = 12
                bigFireButton.layer.borderColor = UIColor.red.cgColor
                
            }
            
            
        
        
        
  
    }
    

}






